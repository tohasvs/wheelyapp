package app.wheely.com.wheelyapp.service.handler;

public interface DisconnectListener {
    void onDisconnect();
}

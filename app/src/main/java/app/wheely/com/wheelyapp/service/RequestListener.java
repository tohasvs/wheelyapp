package app.wheely.com.wheelyapp.service;

import android.os.Bundle;

public interface RequestListener {
    public abstract void onCompleted(int code, Bundle responseData);
}

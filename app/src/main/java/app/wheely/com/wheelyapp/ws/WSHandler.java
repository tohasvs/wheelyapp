package app.wheely.com.wheelyapp.ws;

public interface WSHandler {
    void onOpen();
    void onClose(int code, String reason);
    void onTextMessage(String payload);
}

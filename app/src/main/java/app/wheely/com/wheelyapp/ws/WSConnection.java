package app.wheely.com.wheelyapp.ws;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

public class WSConnection implements IWSConnection {

    private final WebSocketConnection mConnection;

    public WSConnection() {
        this.mConnection = new WebSocketConnection();
    }

    @Override
    public void connect(String wsUri, WSHandler wsHandler) {
        try {
            mConnection.connect(wsUri, new WHandler(wsHandler));
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isConnected() {
        return mConnection.isConnected();
    }

    @Override
    public void sendTextMessage(String payload) {
        mConnection.sendTextMessage(payload);
    }

    @Override
    public void disconnect() {
        mConnection.disconnect();
    }

    private class WHandler extends WebSocketHandler {

        private WSHandler mWSHandler;

        private WHandler(WSHandler wsHandler) {
            mWSHandler = wsHandler;
        }

        @Override
        public void onOpen() {
            super.onOpen();
            mWSHandler.onOpen();
        }

        @Override
        public void onClose(int code, String reason) {
            super.onClose(code, reason);
            mWSHandler.onClose(code, reason);
        }

        @Override
        public void onTextMessage(String payload) {
            super.onTextMessage(payload);
            mWSHandler.onTextMessage(payload);
        }
    }
}

package app.wheely.com.wheelyapp.service;

public enum ServiceAction {
    AUTHORIZE, SEND_DATA, DISCONNECT;
}

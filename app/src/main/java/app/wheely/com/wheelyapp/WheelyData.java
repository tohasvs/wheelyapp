package app.wheely.com.wheelyapp;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class WheelyData implements Parcelable {

    private Integer mId;
    private LatLng mLocation;

    public static final Parcelable.Creator<WheelyData> CREATOR
            = new Parcelable.Creator<WheelyData>() {
        public WheelyData createFromParcel(Parcel in) {
            return new WheelyData(in);
        }

        public WheelyData[] newArray(int size) {
            return new WheelyData[size];
        }
    };

    public WheelyData(Integer id, LatLng location) {
        this.mId = id;
        this.mLocation = location;
    }

    public WheelyData(Parcel in) {
        mId = in.readInt();
        mLocation = in.readParcelable(Location.class.getClassLoader());
    }

    public Integer getId() {
        return mId;
    }

    public LatLng getLocation() {
        return mLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeParcelable(mLocation, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof WheelyData)) return false;
        WheelyData wheelyData = (WheelyData) o;
        return mLocation.equals(wheelyData.getLocation()) && mId == wheelyData.getId();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + mId;
        result = prime * result + mLocation.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WheelyData{" +
                "id='" + mId + '\'' +
                ", location='" + mLocation.toString() + '\'' +
                '}';
    }
}

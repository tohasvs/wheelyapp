package app.wheely.com.wheelyapp;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.wheely.com.wheelyapp.service.RequestListener;
import app.wheely.com.wheelyapp.service.WeelyServiceHelper;
import app.wheely.com.wheelyapp.service.WheelyHanlder;
import app.wheely.com.wheelyapp.util.NetworkUtil;

public class MapActivity extends FragmentActivity {

    public static final float ZOOM_TO = 14f;
    public static final LatLng MOSCOW_LOCATION = new LatLng(55.75, 37.61);

    private GoogleMap mMap;
    private WeelyServiceHelper mWeelyServiceHelper;

    private GoogleMap.OnMyLocationChangeListener mLocationListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            if (!NetworkUtil.hasInternetConnection(getApplicationContext())) {
                showErrorMessage(R.string.network_unavailable);
                return;
            }

            ((WheelyApplication) getApplication()).getWeelyServiceHelper().sendLocation(location);
        }
    };

    private RequestListener mConnectionListener = new RequestListener() {
        @Override
        public void onCompleted(int code, Bundle responseData) {
            switch (ResponseCode.getCode(code)) {
                case LOCATION_DATA:
                    ArrayList<WheelyData> locations = responseData.getParcelableArrayList(WheelyHanlder.WHEELY_LOCATIONS);
                    addMarkersToMap(locations);
                    break;
                case CLOSE_CONNECTION:
                    stopWheelyService();
                    goToAuthActivity();
                    break;
            }
        }
    };

    private View.OnClickListener mDisconnectListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mWeelyServiceHelper.closeWeelyConnection();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        setTitle(getString(R.string.map_title));

        setUpMapIfNeeded();
        findViewById(R.id.btn_disconnect).setOnClickListener(mDisconnectListener);

        mWeelyServiceHelper = ((WheelyApplication) getApplication()).getWeelyServiceHelper();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWeelyServiceHelper.addListener(mConnectionListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWeelyServiceHelper.removeListener(mConnectionListener);
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(mLocationListener);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(MOSCOW_LOCATION, ZOOM_TO);
        mMap.animateCamera(cameraUpdate);
    }

    private void addMarkersToMap(ArrayList<WheelyData> locations) {
        for (WheelyData location : locations) {
            mMap.addMarker(new MarkerOptions().position(location.getLocation())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        }
    }

    private void showErrorMessage(int message) {
        Toast.makeText(getApplicationContext(), getString(message), Toast.LENGTH_LONG).show();
    }

    private void stopWheelyService() {
        mWeelyServiceHelper.stopWheelyService();
    }

    private void goToAuthActivity() {
        Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
        startActivity(intent);
        finish();
    }
}

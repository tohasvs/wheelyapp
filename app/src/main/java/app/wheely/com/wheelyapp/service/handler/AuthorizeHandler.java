package app.wheely.com.wheelyapp.service.handler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import app.wheely.com.wheelyapp.R;
import app.wheely.com.wheelyapp.ws.WSConnection;
import app.wheely.com.wheelyapp.ws.WSHandler;

public class AuthorizeHandler implements ServiceHandler {
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_PASSWORD = "password";

    private WSHandler mHandler;
    private Context mContext;

    public AuthorizeHandler(Context context, WSHandler handler) {
        mHandler = handler;
        mContext = context;
    }

    @Override
    public void onHandle(WSConnection wsConnection, Intent intent) {
        if (wsConnection.isConnected()) {
            wsConnection.disconnect();
        }

        String wsUri = buildUrl(intent);
        wsConnection.connect(wsUri, mHandler);
    }

    private String buildUrl(Intent intent) {
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme(mContext.getString(R.string.welly_scheme))
                .authority(mContext.getString(R.string.welly_authority));

        uriBuilder.appendQueryParameter(PARAM_USERNAME, intent.getStringExtra(PARAM_USERNAME));
        uriBuilder.appendQueryParameter(PARAM_PASSWORD, intent.getStringExtra(PARAM_PASSWORD));
        return uriBuilder.toString();
    }
}

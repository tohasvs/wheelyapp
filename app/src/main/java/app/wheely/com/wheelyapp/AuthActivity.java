package app.wheely.com.wheelyapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.wheely.com.wheelyapp.service.RequestListener;
import app.wheely.com.wheelyapp.service.WeelyServiceHelper;
import app.wheely.com.wheelyapp.util.NetworkUtil;

public class AuthActivity extends BaseActivity {

    public static final String PREFIX_A = "a";

    private EditText mUsername;
    private EditText mPassword;
    private Button mConnect;

    private WeelyServiceHelper mWeelyServiceHelper;

    private View.OnClickListener mConnectedClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!NetworkUtil.hasInternetConnection(getApplicationContext())) {
                showErrorMessage(R.string.network_unavailable);
                return;
            }

            if (!checkFields()) {
                showErrorMessage(R.string.uncorrect_fields);
                return;
            }

            performAuthorization();
            mConnect.setEnabled(false);
        }
    };

    private RequestListener mConnectionListener = new RequestListener() {
        @Override
        public void onCompleted(int code, Bundle responseData) {
            ResponseCode responseCode = ResponseCode.getCode(code);
            switch (responseCode) {
                case OPEN_CONNECTION:
                    goToMap();
                    removeResponseListener();
                    finish();
                    break;
                case CLOSE_CONNECTION:
                    mConnect.setEnabled(true);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getString(R.string.login_title));

        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);

        mConnect = (Button) findViewById(R.id.btn_connect);
        mConnect.setOnClickListener(mConnectedClick);

        mWeelyServiceHelper = ((WheelyApplication) getApplication()).getWeelyServiceHelper();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWeelyServiceHelper.addListener(mConnectionListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeResponseListener();
    }

    private void removeResponseListener() {
        mWeelyServiceHelper.removeListener(mConnectionListener);
    }

    private boolean checkFields() {
        return isTextFieldRight(mUsername.getText().toString())
                && isTextFieldRight(mPassword.getText().toString());
    }

    private boolean isTextFieldRight(String text) {
        return !TextUtils.isEmpty(text) && text.startsWith(PREFIX_A);
    }

    private void showErrorMessage(int message) {
        Toast.makeText(getApplicationContext(), getString(message), Toast.LENGTH_LONG).show();
    }

    private void performAuthorization() {
        mWeelyServiceHelper.openWeelyConnection(mUsername.getText().toString(),
                mPassword.getText().toString());
    }

    private void goToMap() {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }
}

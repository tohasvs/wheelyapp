package app.wheely.com.wheelyapp.service;

import android.content.Intent;
import android.os.Bundle;

import app.wheely.com.wheelyapp.ResponseCode;
import app.wheely.com.wheelyapp.ws.WSConnection;
import app.wheely.com.wheelyapp.service.handler.DisconnectListener;
import app.wheely.com.wheelyapp.service.handler.HandleFactory;
import app.wheely.com.wheelyapp.service.handler.ServiceHandler;
import de.greenrobot.event.EventBus;

interface ServiceResponse {
    void sendResponse(ResponseCode code, Bundle data);
    void sendResponse(ResponseCode code, String data);
}

public class WeelyService extends KeepAliveService implements ServiceResponse {

    public static final String EXTRA_ACTION = "action";
    public static final String EXTRA_RESPONSE = "response";
    public static final String RESPONSE_CODE = "responseCode";
    public static final String AUTHENTICATION_FAILED = "Authentication failed";

    private WSConnection mWSConnection;
    private WheelyHanlder mWheelyHanlder;

    private DisconnectListener mDisconnectListener = new DisconnectListener() {
        @Override
        public void onDisconnect() {
            sendResponse(ResponseCode.CLOSE_CONNECTION, AUTHENTICATION_FAILED);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mWSConnection = new WSConnection();
        mWheelyHanlder = new WheelyHanlder(this);
    }

    @Override
    void onHandleIntent(Intent intent) {
        if (intent != null) {
            ServiceHandler serviceHandler = HandleFactory.makeHanlder(getApplicationContext(),
                    getAction(intent), mWheelyHanlder, mDisconnectListener);
            serviceHandler.onHandle(mWSConnection, intent);
        }
    }

    private ServiceAction getAction(Intent intent) {
        return ((ServiceAction) intent.getSerializableExtra(EXTRA_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSConnection.isConnected()) {
            mWSConnection.disconnect();
        }
    }

    @Override
    public void sendResponse(ResponseCode code, Bundle data) {
        data.putInt(RESPONSE_CODE, code.getCode());
        EventBus.getDefault().post(data);
    }

    @Override
    public void sendResponse(ResponseCode code, String data) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_RESPONSE, data);
        sendResponse(code, bundle);
    }
}

package app.wheely.com.wheelyapp.service.handler;

import android.content.Intent;

import app.wheely.com.wheelyapp.ws.WSConnection;

public interface ServiceHandler {
    void onHandle(WSConnection wsConnection, Intent intent);
}

package app.wheely.com.wheelyapp.service.handler;

import android.content.Intent;

import app.wheely.com.wheelyapp.ws.WSConnection;

public class DisconnectHandler implements ServiceHandler {
    @Override
    public void onHandle(WSConnection wsConnection, Intent intent) {
        if (wsConnection.isConnected()) {
            wsConnection.disconnect();
        }
    }
}

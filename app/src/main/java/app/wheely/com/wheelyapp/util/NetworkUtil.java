package app.wheely.com.wheelyapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class NetworkUtil {
    public enum NetworkState {
        WIFI, MOBILE, NONE;
    }

    public static boolean hasInternetConnection(Context context) {
        NetworkState networkState = getState(context);
        if (networkState == NetworkState.MOBILE || networkState == NetworkState.WIFI) {
            return true;
        }
        return false;
    }

    public static NetworkState getState(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    return NetworkState.WIFI;
                case ConnectivityManager.TYPE_MOBILE:
                    return NetworkState.MOBILE;
            }
        }
        return NetworkState.NONE;
    }
}

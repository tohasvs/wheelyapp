package app.wheely.com.wheelyapp.service.handler;

import android.content.Context;

import app.wheely.com.wheelyapp.service.ServiceAction;
import app.wheely.com.wheelyapp.service.WheelyHanlder;
import app.wheely.com.wheelyapp.service.handler.AuthorizeHandler;
import app.wheely.com.wheelyapp.service.handler.DefaultHandler;
import app.wheely.com.wheelyapp.service.handler.LocationHandler;
import app.wheely.com.wheelyapp.service.handler.ServiceHandler;

public class HandleFactory {

    public static ServiceHandler makeHanlder(Context context, ServiceAction serviceAction,
                                WheelyHanlder wsHanlder, DisconnectListener disconnectListener) {
        switch (serviceAction) {
            case AUTHORIZE:
                return new AuthorizeHandler(context, wsHanlder);
            case SEND_DATA:
                return new LocationHandler(disconnectListener);
            case DISCONNECT:
                return new DisconnectHandler();
        }
        return new DefaultHandler();
    }
}
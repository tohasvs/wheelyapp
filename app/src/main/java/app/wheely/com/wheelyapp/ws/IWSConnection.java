package app.wheely.com.wheelyapp.ws;

public interface IWSConnection {
    void connect(String wsUri, WSHandler wsHandler);
    boolean isConnected();
    void sendTextMessage(String payload);
    public void disconnect();
}

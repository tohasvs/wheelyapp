package app.wheely.com.wheelyapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

public abstract class KeepAliveService extends Service {
    public static final String WHEELY_THREAD_NAME = "WheelyThread";

    private volatile ServiceThreadHandler mServiceHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        createThreadHandler();
    }

    private void createThreadHandler() {
        HandlerThread thread = new HandlerThread(WHEELY_THREAD_NAME);
        thread.start();

        mServiceHandler = new ServiceThreadHandler(thread.getLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendMessageToThread(intent);
        return START_STICKY;
    }

    private void sendMessageToThread(Intent intent) {
        Message msg = mServiceHandler.obtainMessage();
        msg.obj = intent;
        mServiceHandler.sendMessage(msg);
    }

    abstract void onHandleIntent(Intent intent);

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceThreadHandler extends Handler {
        public ServiceThreadHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            onHandleIntent((Intent) msg.obj);
        }
    }
}

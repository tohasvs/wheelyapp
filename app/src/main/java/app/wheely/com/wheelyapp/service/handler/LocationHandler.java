package app.wheely.com.wheelyapp.service.handler;

import android.content.Intent;
import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

import app.wheely.com.wheelyapp.ws.WSConnection;

public class LocationHandler implements ServiceHandler {
    public static final String PARAM_LOCATION = "location";

    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lon";

    private DisconnectListener mDisconnectListener;

    public LocationHandler(DisconnectListener disconnectListener) {
        this.mDisconnectListener = disconnectListener;
    }

    @Override
    public void onHandle(WSConnection wsConnection, Intent intent) {
        if (wsConnection.isConnected()) {
            sendLocation(wsConnection, intent);
        } else {
            mDisconnectListener.onDisconnect();
        }
    }

    private void sendLocation(WSConnection wsConnection, Intent intent) {
        try {
            wsConnection.sendTextMessage(getSenderData(intent));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getSenderData(Intent intent) throws JSONException {
        Location location = intent.getParcelableExtra(PARAM_LOCATION);

        JSONObject loc = new JSONObject();
        loc.put(PARAM_LATITUDE, location.getLatitude());
        loc.put(PARAM_LONGITUDE, location.getLongitude());
        return loc.toString();
    }
}

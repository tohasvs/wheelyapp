package app.wheely.com.wheelyapp;

import android.app.Application;

import app.wheely.com.wheelyapp.service.WeelyServiceHelper;

public class WheelyApplication extends Application {
    private WeelyServiceHelper mWeelyServiceHelper;

    public WeelyServiceHelper getWeelyServiceHelper() {
        if (mWeelyServiceHelper == null) {
            mWeelyServiceHelper = new WeelyServiceHelper(this);
        }
        return mWeelyServiceHelper;
    }
}

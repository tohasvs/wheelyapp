package app.wheely.com.wheelyapp;

public enum ResponseCode {
    OPEN_CONNECTION(100), LOCATION_DATA(101), CLOSE_CONNECTION(103), DEFAULT(-1);

    private int mCode;

    ResponseCode(int code) {
        this.mCode = code;
    }

    public Integer getCode() {
        return mCode;
    }

    public static ResponseCode getCode(int code) {
        for (ResponseCode responseCode : values()) {
            if (responseCode.getCode() == code) {
                return responseCode;
            }
        }
        return DEFAULT;
    }
}

package app.wheely.com.wheelyapp.service;

import android.app.Application;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import app.wheely.com.wheelyapp.service.handler.AuthorizeHandler;
import app.wheely.com.wheelyapp.service.handler.LocationHandler;

public class WeelyServiceHelper extends ServiceHelper {

    public WeelyServiceHelper(Application application) {
        super(application);
    }

    public void openWeelyConnection(String username, String password) {
        Intent intent = getIntentService(ServiceAction.AUTHORIZE);
        intent.putExtra(AuthorizeHandler.PARAM_USERNAME, username);
        intent.putExtra(AuthorizeHandler.PARAM_PASSWORD, password);
        startCommandService(intent);
    }

    public void closeWeelyConnection() {
        Intent intent = getIntentService(ServiceAction.DISCONNECT);
        startCommandService(intent);
    }

    public void stopWheelyService() {
        Intent intent = new Intent(getContext(), WeelyService.class);
        getContext().stopService(intent);
    }

    public void sendLocation(Location location) {
        Intent intent = getIntentService(ServiceAction.SEND_DATA);
        intent.putExtra(LocationHandler.PARAM_LOCATION, location);
        startCommandService(intent);
    }
}

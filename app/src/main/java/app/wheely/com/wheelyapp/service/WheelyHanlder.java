package app.wheely.com.wheelyapp.service;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.wheely.com.wheelyapp.ResponseCode;
import app.wheely.com.wheelyapp.WheelyData;
import app.wheely.com.wheelyapp.ws.WSHandler;

public class WheelyHanlder implements WSHandler {

    public static final String PARAM_ID = "id";
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lon";
    public static final String WHEELY_LOCATIONS = "wheelyLocations";

    private ServiceResponse mServiceResponse;

    WheelyHanlder(ServiceResponse serviceResponse) {
        mServiceResponse = serviceResponse;
    }

    @Override
    public void onOpen() {
        mServiceResponse.sendResponse(ResponseCode.OPEN_CONNECTION, new Bundle());
    }

    @Override
    public void onClose(int code, String reason) {
        mServiceResponse.sendResponse(ResponseCode.CLOSE_CONNECTION, reason);
    }

    @Override
    public void onTextMessage(String payload) {
        try {
            Bundle data = new Bundle();
            ArrayList<WheelyData> wheelyLocations = getWheelyLocations(payload);
            data.putParcelableArrayList(WHEELY_LOCATIONS, wheelyLocations);
            mServiceResponse.sendResponse(ResponseCode.LOCATION_DATA, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<WheelyData> getWheelyLocations(String payload) throws JSONException {
        ArrayList<WheelyData> wheelyLocations = new ArrayList<WheelyData>();
        JSONArray arrLocations = new JSONArray(payload);
        for (int i = 0; i < arrLocations.length(); i++) {
            JSONObject loc = arrLocations.getJSONObject(i);

            wheelyLocations.add(new WheelyData(loc.getInt(PARAM_ID),
                    new LatLng(loc.getDouble(PARAM_LATITUDE), loc.getDouble(PARAM_LONGITUDE))));
        }
        return wheelyLocations;
    }
}

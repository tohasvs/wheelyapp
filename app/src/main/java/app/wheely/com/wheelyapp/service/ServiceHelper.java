package app.wheely.com.wheelyapp.service;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class ServiceHelper {
    private List<RequestListener> mCurrentListeners;

    private Application mApp;

    public ServiceHelper(Application application) {
        mApp = application;
        mCurrentListeners = new ArrayList<RequestListener>();
        EventBus.getDefault().register(this);
    }

    public Application getApp() {
        return mApp;
    }

    public Context getContext() {
        return mApp.getApplicationContext();
    }

    public void addListener(RequestListener listener) {
        mCurrentListeners.add(listener);
    }

    public void removeListener(RequestListener listener) {
        mCurrentListeners.remove(listener);
    }

    protected void startCommandService(Intent intent) {
        getContext().startService(intent);
    }

    protected Intent getIntentService(ServiceAction action) {
        Intent intent = new Intent(getContext(), WeelyService.class);
        intent.putExtra(WeelyService.EXTRA_ACTION, action);
        return intent;
    }

    public void onEvent(Bundle data) {
        for (RequestListener listener : mCurrentListeners) {
            if (listener != null) {
                sendResponse(getResponseCode(data), data, listener);
            }
        }
    }

    private int getResponseCode(Bundle data) {
        return data.getInt(WeelyService.RESPONSE_CODE);
    }

    private void sendResponse(int resultCode, Bundle resultData, RequestListener listener) {
        listener.onCompleted(resultCode, resultData);
    }
}

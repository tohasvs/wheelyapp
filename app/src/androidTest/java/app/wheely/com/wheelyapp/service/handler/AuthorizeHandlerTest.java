package app.wheely.com.wheelyapp.service.handler;

import android.content.Intent;
import android.test.AndroidTestCase;

import app.wheely.com.wheelyapp.ws.WSHandler;

public class AuthorizeHandlerTest extends AndroidTestCase {

    public static final String DEF_USERNAME = "ausername";
    public static final String DEF_PASSWORD = "apassword";
    private AuthorizeHandler mAuthorizeHandler;
    private WSHandler mWSHandler = new WSHandler() {
        @Override
        public void onOpen() {

        }

        @Override
        public void onClose(int code, String reason) {

        }

        @Override
        public void onTextMessage(String payload) {

        }
    };


    public void setUp() throws Exception {
        mAuthorizeHandler = new AuthorizeHandler(getContext(), mWSHandler);
    }

    public void testOnHandleConnected() throws Exception {
        MockWSConnection wsConnection = new MockWSConnection();
        wsConnection.setConnected(false);
        mAuthorizeHandler.onHandle(wsConnection, getIntent());

        assertTrue(wsConnection.isConnectCalled);
        assertFalse(wsConnection.isDisconectCalled);
    }

    public void testOnHandleDisconnected() throws Exception {
        MockWSConnection wsConnection = new MockWSConnection();
        wsConnection.setConnected(true);
        mAuthorizeHandler.onHandle(wsConnection, getIntent());

        assertTrue(wsConnection.isConnectCalled);
        assertTrue(wsConnection.isDisconectCalled);
    }

    private Intent getIntent() {
        Intent intent = new Intent();
        intent.putExtra(AuthorizeHandler.PARAM_USERNAME, DEF_USERNAME);
        intent.putExtra(AuthorizeHandler.PARAM_PASSWORD, DEF_PASSWORD);
        return intent;
    }
}
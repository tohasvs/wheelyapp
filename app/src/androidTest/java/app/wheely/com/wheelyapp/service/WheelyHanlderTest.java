package app.wheely.com.wheelyapp.service;


import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.wheely.com.wheelyapp.ResponseCode;
import app.wheely.com.wheelyapp.WheelyData;

public class WheelyHanlderTest extends TestCase {

    public static final int NUM = 4;

    public static final int DEF_ID = 1;
    public static final double DEF_LATITUDE = 55.64;
    public static final double DEF_LONGITUDE = 37.55;
    public static final int DEF_CODE = 500;
    public static final String DEF_REASON = "REASON";

    private MockServiceResponse mServiceResponse;
    private WheelyHanlder mWheelyHanlder;

    public void setUp() throws Exception {
        mServiceResponse = new MockServiceResponse();
        mWheelyHanlder = new WheelyHanlder(mServiceResponse);
    }

    public void testOnOpen() throws Exception {
        mWheelyHanlder.onOpen();

        assertEquals(mServiceResponse.execCount, 1);
        assertEquals(mServiceResponse.responseCode, ResponseCode.OPEN_CONNECTION);
    }

    public void testOnClose() throws Exception {
        mWheelyHanlder.onClose(DEF_CODE, DEF_REASON);

        assertEquals(mServiceResponse.execCount, 1);
        assertEquals(mServiceResponse.responseCode, ResponseCode.CLOSE_CONNECTION);
        assertEquals(mServiceResponse.dataString, DEF_REASON);
    }

    public void testOnTextMessage() throws Exception {
        mWheelyHanlder.onTextMessage(generateJSON(DEF_ID, DEF_LATITUDE, DEF_LONGITUDE));
        assertEquals(mServiceResponse.execCount, 1);
        assertEquals(mServiceResponse.responseCode, ResponseCode.LOCATION_DATA);

        assertNotNull(mServiceResponse.dataBundle);
        assertTrue(mServiceResponse.dataBundle.containsKey(WheelyHanlder.WHEELY_LOCATIONS));

        ArrayList<WheelyData> locations = mServiceResponse
                .dataBundle.getParcelableArrayList(WheelyHanlder.WHEELY_LOCATIONS);
        assertTrue(checkLocations(locations));
    }

    private String generateJSON(int id, Double lat, Double lng) {
        JSONArray arr = new JSONArray();
        for (int i = 0; i < NUM; i++) {
            try {
                JSONObject jsObj = new JSONObject();
                jsObj.put(WheelyHanlder.PARAM_ID, id);
                jsObj.put(WheelyHanlder.PARAM_LATITUDE, lat);
                jsObj.put(WheelyHanlder.PARAM_LONGITUDE, lng);
                arr.put(jsObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arr.toString();
    }

    private boolean checkLocations(ArrayList<WheelyData> arrWheelyData) {
        LatLng defLoc = new LatLng(DEF_LATITUDE, DEF_LONGITUDE);
        for (WheelyData data : arrWheelyData) {
            if (data.getId() != DEF_ID || !data.getLocation().equals(defLoc)) {
                return false;
            }
        }
        return true;
    }

    static class MockServiceResponse implements ServiceResponse {
        int execCount;
        ResponseCode responseCode;
        Bundle dataBundle;
        String dataString;

        @Override
        public void sendResponse(ResponseCode code, Bundle data) {
            this.execCount++;
            this.responseCode = code;
            this.dataBundle = data;
        }

        @Override
        public void sendResponse(ResponseCode code, String data) {
            this.execCount++;
            this.responseCode = code;
            this.dataString = data;
        }
    }

}
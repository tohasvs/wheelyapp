package app.wheely.com.wheelyapp.service.handler;

import app.wheely.com.wheelyapp.ws.WSConnection;
import app.wheely.com.wheelyapp.ws.WSHandler;

public class MockWSConnection extends WSConnection {
    private boolean isConnected;

    boolean isDisconectCalled;
    boolean isConnectCalled;
    boolean isTextMessageCalled;
    String payload;

    @Override
    public void connect(String wsUri, WSHandler wsHandler) {
        isConnectCalled = true;
    }

    @Override
    public void disconnect() {
        isDisconectCalled = true;
    }

    @Override
    public void sendTextMessage(String payload) {
        this.isTextMessageCalled = true;
        this.payload = payload;
    }

    public void setConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }
}
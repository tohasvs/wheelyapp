package app.wheely.com.wheelyapp.service.handler;

import android.content.Intent;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import junit.framework.TestCase;

public class LocationHandlerTest extends TestCase {
    public static final LatLng MOSCOW_LOCATION = new LatLng(55.75, 37.61);

    private LocationHandler mLocationHandler;
    private MockDisconnectListener mMockDisconnectListener;
    private MockWSConnection mWSConnection;

    public void setUp() throws Exception {
        super.setUp();
        mWSConnection = new MockWSConnection();
        mMockDisconnectListener = new MockDisconnectListener();
        mLocationHandler = new LocationHandler(mMockDisconnectListener);
    }

    public void testSendLocation() throws Exception {
        Intent intent = getLocationIntent();
        mWSConnection.setConnected(true);
        mLocationHandler.onHandle(mWSConnection, intent);

        assertTrue(mWSConnection.isTextMessageCalled);
        assertTrue(!mWSConnection.payload.equals(""));
    }

    private Intent getLocationIntent() {
        Intent intent = new Intent();
        Location location = new Location("");
        location.setLatitude(MOSCOW_LOCATION.latitude);
        location.setLongitude(MOSCOW_LOCATION.longitude);
        intent.putExtra(LocationHandler.PARAM_LOCATION, location);
        return intent;
    }

    public void testDisconnectListener() throws Exception {
        mWSConnection.setConnected(false);
        mLocationHandler.onHandle(mWSConnection, null);

        assertEquals(mMockDisconnectListener.execCount, 1);
    }

    static class MockDisconnectListener implements DisconnectListener {
        int execCount;

        @Override
        public void onDisconnect() {
            execCount++;
        }
    }
}